# lioness

* В папке, где располагаются файлы lioness.py и delioness.py
создать файл с расширением input.txt. 

* Записать в этот файл текстовые данные

* Зашифровать файл input.txt командой python lioness.py input.txt. 
В результате выполнения команды должен появиться файл
encrypted.txt, содержащий зашифрованные данные.

* Расшифровать файл encrypted.txt командой python delioness.py encrypted.txt.
В результате выполнения комады должен появиться файл decrypted.txt, 
содержимое которого совпадает с input.txt.

>>> python lioness.py input.txt

>>> python delioness.py encrypted.txt

