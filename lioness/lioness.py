import os
import sys

from hashlib import md5

from utils import gsch, k1, k2, k3, k4

filename = sys.argv[1]

with open(filename, 'br') as in_file, \
        open('encrypted.txt', 'bw') as out_file:

    sizef = os.path.getsize(filename)

    while sizef > 0:
        str1 = in_file.read(32)
        str2 = in_file.read(1024 - 32)

        l = int.from_bytes(str1, 'little')
        r = int.from_bytes(str2, 'little')
        chk1 = int.from_bytes(k1, 'little')
        chk2 = int.from_bytes(k2, 'little')
        chk3 = int.from_bytes(k3, 'little')
        chk4 = int.from_bytes(k4, 'little')
        # 1
        lv = l ^ chk1
        lv = gsch(lv)
        r = r ^ lv
        r = r.to_bytes(1024 - 32, 'little')
        # 2
        rv = md5(k2 + r + k2).hexdigest()
        rv = rv.encode('ascii')
        rv = int.from_bytes(rv, 'little')
        l = l ^ rv
        # 3
        lv = l ^ chk3
        lv = gsch(lv)
        r = int.from_bytes(r, 'little')
        r = r ^ lv
        r = r.to_bytes(1024 - 32, 'little')
        # 4
        rv = md5(k4 + r + k4).hexdigest()
        rv = rv.encode('ascii')
        rv = int.from_bytes(rv, 'little')
        l = l ^ rv

        str1 = l.to_bytes(32, 'little')
        str2 = r

        out_file.write(str1)  # Р·Р°РїРёСЃСЊ РІ С„Р°Р№Р»
        out_file.write(str2)

        sizef -= 1024
