k1 = b'1234qwer1234qwer1234qwer1234qwer1234qwer1234qwer1234qwer1234qwer1234qwer1234q'
k2 = b'lapinovazachetlapinovazachetlapinovazachetlapinovazachetlapinovazachetlapinov'
k3 = b'LIohaKRasavaLIohaKRasavaLIohaKRasavaLIohaKRasavaLIohaKRasavaLIohaKRasavaLIoha'
k4 = b'babloplatybabloplatybabloplatybabloplatybabloplatybabloplatybabloplatybablopl'


def gsch(x):
    y = 362436068
    z = 521288628
    w = 88675122

    i = 0
    while (i != 2000):
        t = x ^ (x << 10)
        x = y
        y = z
        z = w
        w ^= w >> 18
        t ^= t >> 7
        w ^= t
        x += ((9 ** 8) * w // (10 ** (len(str(w)))))
        i += 1

    print('key ' + ' is   ' + str(x))

    return x
